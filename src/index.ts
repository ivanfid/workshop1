const argv = require('yargs').argv;

let originalBet: number = argv.bet;
let bet: number = originalBet

let originalBalance: number = argv.balance;
let balance: number = originalBalance;

let gameOver: boolean = false;
let roundCounter: number = 0;

startGame();

function startGame()
{
	while (!gameOver)
	{
		if (checkSpinOutcome())
		{
			balance += bet;
			bet = originalBet;
		}
		else
		{
			balance -= bet;
			bet *= 2;
		}

		roundCounter++;

		if (balance > (originalBalance * 1.2))
		{
			gameOver = true;
			console.log("Won in", roundCounter, "rounds");
			roundCounter = 0;
		}
	}
}

function checkSpinOutcome(): boolean
{
	let chance: number = Math.random();
	let win: boolean;
	if (chance <= (18 / 38))
	{
		win = true;
	}
	else
	{
		win = false;
	}

	return win;
}




