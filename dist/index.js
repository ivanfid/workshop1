const argv = require('yargs').argv;
let originalBet = argv.bet;
let bet = originalBet;
let originalBalance = argv.balance;
let balance = originalBalance;
let gameOver = false;
let roundCounter = 0;
startGame();
function startGame() {
    while (!gameOver) {
        if (checkSpinOutcome()) {
            balance += bet;
            bet = originalBet;
        }
        else {
            balance -= bet;
            bet *= 2;
        }
        roundCounter++;
        if (balance > (originalBalance * 1.2)) {
            gameOver = true;
            console.log("Won in", roundCounter, "rounds");
            roundCounter = 0;
        }
    }
}
function checkSpinOutcome() {
    let chance = Math.random();
    let win;
    if (chance <= (18 / 37)) {
        win = true;
    }
    else {
        win = false;
    }
    return win;
}
